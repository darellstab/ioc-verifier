import React from 'react'
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import FavoriteIcon from '@mui/icons-material/Favorite';


export function ProductCard({name, description, imageUrl, addToBasket, addFavourite}) {

    function cropText(text, numberOfChars) {
        return text.substring(0, numberOfChars) + ' ...';
    }

    return (
            <Card sx={{ maxWidth: 345 }}>
                <CardMedia
                        component="img"
                        height="140"
                        image={imageUrl}
                        alt={name}
                />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                        {name}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                        {cropText(description, 90)}
                    </Typography>
                </CardContent>
                <CardActions>
                    <Button size="small" onClick={addToBasket}>Add to Basket</Button>
                    <IconButton edge="end" aria-label="favourite" onClick={() => addFavourite(name)}>
                        <FavoriteIcon />
                    </IconButton>
                </CardActions>
            </Card>
    );
}
