import React, {useState} from 'react'
import Modal from '@mui/material/Modal';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import JSONPretty from 'react-json-pretty';

export function VerifiablePresentationModal({title, verifiablePresentation}) {

    const [open, setOpen] = useState(false);

    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 1400,
        height: '90vh',
        bgcolor: 'background.paper',
        border: '2px solid #000',
        boxShadow: 24,
        p: 4,
    };

    return (
            <>
                <Button onClick={() => setOpen(true)}>Show credentials</Button>
                <Modal
                    open={open}
                    onClose={() => setOpen(false)}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                >
                    <Box sx={style}>
                        <Typography id="modal-modal-title" variant="h6" component="h2">
                            {title}
                        </Typography>
                        <div style={{width: '100%', height: '95%', overflowY: 'auto'}}>
                            <div>
                                <JSONPretty data={verifiablePresentation} />
                            </div>
                        </div>
                        <Button onClick={() => setOpen(false)}>Close</Button>
                    </Box>
                </Modal>
            </>
    );
}
