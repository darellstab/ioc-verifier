import React from 'react'
import Box from '@mui/material/Container';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import IconButton from '@mui/material/IconButton';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import ListItemText from '@mui/material/ListItemText';
import FolderIcon from '@mui/icons-material/Folder';
import DeleteIcon from '@mui/icons-material/Delete';
import Avatar from '@mui/material/Avatar';
import {Link as RouterLink} from 'react-router-dom';



export function Basket({basket, setBasket}) {

    if(basket.length === 0) {
        return;
    }

    function remove(productToRemove) {
        setBasket(basket.filter(product => {
            return productToRemove.id !== product.id
        }));
    }

    return (
            <Box sx={{ width: 400 }}>
                <Typography gutterBottom variant="h5" component="div">
                    Basket
                </Typography>
                <List>
                    {basket && basket.map(product => {
                        return (
                                <ListItem key={product.id}
                                        secondaryAction={
                                            <IconButton edge="end" aria-label="delete" onClick={() => remove(product)}>
                                                <DeleteIcon />
                                            </IconButton>
                                        }
                                >
                                    <ListItemAvatar>
                                        <Avatar>
                                            <FolderIcon />
                                        </Avatar>
                                    </ListItemAvatar>
                                    <ListItemText
                                            primary={product.name}
                                    />
                                </ListItem>
                        );
                    })}
                </List>
                <Button to="/checkout" component={RouterLink}>Checkout</Button>
            </Box>
    );
}
