import React from 'react'
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import Stack from '@mui/material/Stack';
import Alert from '@mui/material/Alert';
import {VerifiablePresentationModal} from "./VerifiablePresentationModal";


export function NeededCredentialsBox({title, reason, getFromWallet, providedCredentials, verificationFeedback}) {

    const validationErrorStyles = {
        borderColor: 'red'
    };

    return (
        <Grid item xs={6}>
            <Card style={verificationFeedback && providedCredentials.data.id in verificationFeedback.errors ? validationErrorStyles : null}>
                <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                        {title}
                    </Typography>
                    <Typography variant="subtitle1" gutterBottom component="div">
                        {reason}
                    </Typography>
                </CardContent>
                <CardActions>
                    <Stack spacing={2} direction="row">
                        <Button variant="contained" onClick={getFromWallet}>Provide from Wallet </Button>
                        <Button variant="contained" disabled>Provide manually</Button>
                    </Stack>
                </CardActions>
                {providedCredentials && <VerifiablePresentationModal verifiablePresentation={providedCredentials} title={title + ' Credentials'} /> }
                {verificationFeedback &&
                        providedCredentials.data.id in verificationFeedback.errors &&
                        <Alert style={{margin: '5px'}} severity="error">{JSON.stringify(verificationFeedback.errors[providedCredentials.data.id])}</Alert>
                }
            </Card>
        </Grid>
    );
}
