

export function getCredentialQuery(credentialType) {
    return {
        "web": {
            "VerifiablePresentation": {
                "query": [{
                    "type": "QueryByExample",
                    "credentialQuery": {
                        "reason": "Please present a " + credentialType,
                        "example": {
                            "@context": [
                                "https://w3id.org/credentials/v1",
                                "https://www.w3.org/2018/credentials/examples/v1"
                            ],
                            "type": [credentialType],
                        }
                    }
                }]
            },
            "recommendedHandlerOrigins": [
                "https://chapi-demo-wallet.digitalbazaar.com"
            ]
        }
    };
}
