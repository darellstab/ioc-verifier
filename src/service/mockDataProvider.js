
const placeHolderImage = 'https://via.placeholder.com/300x200';

export function getProducts() {
    return [
        {
            id: 1,
            name: 'Product A',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vehicula justo a purus convallis, sit amet semper nisl accumsan.' ,
            category: 'Topic A',
            imageUrl: placeHolderImage
        },
        {
            id: 2,
            name: 'Product B',
            description: 'In id turpis non nulla rutrum suscipit. Nulla in justo vestibulum, lacinia tellus in, aliquam arcu. Donec sit amet massa eget turpis scelerisque efficitur. ',
            category: 'Topic B',
            imageUrl: placeHolderImage
        },
        {
            id: 3,
            name: 'Product C',
            description: 'Quisque eget tortor eu justo auctor sagittis. Vivamus mollis sollicitudin odio, malesuada aliquet elit fermentum fermentum. Maecenas interdum nibh a laoreet laoreet.',
            category: 'Topic C',
            imageUrl: placeHolderImage
        },
        {
            id: 4,
            name: 'Product D',
            description: 'Etiam aliquam, magna a egestas auctor, arcu odio congue nunc, convallis accumsan neque eros at nisi. Sed feugiat libero quis velit condimentum aliquet.',
            category: 'Topic D',
            imageUrl: placeHolderImage
        },
        {
            id: 5,
            name: 'Product E',
            description: 'Proin at mattis lorem. Praesent nec risus odio. Sed pellentesque pretium lacus, eu dictum risus venenatis ac.',
            category: 'Topic E',
            imageUrl: placeHolderImage
        },
        {
            id: 6,
            name: 'Product F',
            description: 'Curabitur faucibus ipsum lorem, sit amet rhoncus dui consectetur vitae. Suspendisse vel tincidunt lacus. Curabitur metus sem, porta ut elit sed, ultricies aliquet augue.',
            category: 'Topic F',
            imageUrl: placeHolderImage
        },
    ];
}

export function getProductById(id) {
    return getProducts().find(product => product.id === id);
}
