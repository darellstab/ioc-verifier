
import axios from 'axios';

const baseUrl = process.env.REACT_APP_BACKEND_URL;
const placeOrderEndpoint = 'orders/place/';

export function sendOrder(basket, vps) {
    return axios.put(baseUrl + placeOrderEndpoint, {basket: basket, vps: JSON.stringify(vps)})
            .then(response => response.data);
}
