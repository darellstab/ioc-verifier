import {getCredentialQuery} from "./presentationRequestService";
import * as polyfill from 'credential-handler-polyfill';


export async function requestCredentials(credentialType) {
    return await navigator.credentials.get(getCredentialQuery(credentialType));
}

export async function setPreferences(preferences, type, presentationType = 'PreferencePresentation') {

    const data = {
        id: window.location.host + Date.now(),
        type: type,
        "@context": [],
        verifiableCredential: {
            ...preferences
        },
    };

    const webCredentialWrapper = new polyfill.WebCredential(
            presentationType,
            data,
            {recommendedHandlerOrigins: [
                    'https://chapi-demo-wallet.digitalbazaar.com'
            ]}
    );

    console.log(webCredentialWrapper);

    return await navigator.credentials.store(webCredentialWrapper);
}
