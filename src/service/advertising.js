

export function getAdContent(topics) {
    if(topics.length === 0) {
        return 'This is a generic context-specific ad.'
    }

    return 'This is a personalised ad, targeting somone with interests in ' + topics.map(topic => topic.title).join(', ');
}
