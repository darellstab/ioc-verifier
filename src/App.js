import './App.css';
import {PageRoutes} from "./pages/PageRoutes";
import useLocalStorage from 'react-use-localstorage';
import {useState} from 'react';
import {setPreferences} from "./service/chapiService";
import {getProductById} from "./service/mockDataProvider";


function App() {

    const [basket, setBasket] = useLocalStorage('ioc-shop-basket', JSON.stringify([]));
    const [topics, setTopics] = useState([]);
    const [sessionId, setSessionId] = useState('');

    function addFavourite(id) {
        if(!topics.some(topic => topic.id === id)) {

            const product = getProductById(id);
            const newTopics = [...topics, {id: id, title: product.category}];
            setPreferences({topics: newTopics}, 'TopicsPreference')
                    .then(response =>             setTopics(newTopics));
        }
    }

    return (
            <div className="IoCVerifier">
                <PageRoutes
                        basket={JSON.parse(basket)}
                        setBasket={basket => setBasket(JSON.stringify(basket))}
                        topics={topics}
                        setTopics={setTopics}
                        addFavourite={addFavourite}
                        sessionId={sessionId}
                        setSessionId={setSessionId}
                />
            </div>
    );
}

export default App;
