import React,{useState,useEffect} from 'react'
import {Outlet} from 'react-router-dom'
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Switch from '@mui/material/Switch';
import Button from '@mui/material/Button';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import QuestionMarkIcon from '@mui/icons-material/QuestionMark';
import AutoGraphIcon from '@mui/icons-material/AutoGraph';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import {Basket} from "../components/Basket";
import {requestCredentials, setPreferences} from "../service/chapiService";
import * as polyfill from 'credential-handler-polyfill';
import {getAdContent} from "../service/advertising";

const MEDIATOR = 'https://authn.io/mediator' + '?origin=' +
        encodeURIComponent(window.location.origin);

export function PageLayout({basket, setBasket, topics, setTopics, sessionId, setSessionId}) {

    const [darkMode, setDarkMode] = useState(false);

    useEffect(() => {
        polyfill
                .loadOnce(MEDIATOR)
                .then(() => {
                    requestCredentials('ThemePreference')
                            .then((response) => {
                                console.log(response);
                                if(response.data.darkMode !== undefined) {
                                    setDarkMode(response.data.darkMode);
                                }
                                if(response.data.topics !== undefined && response.data.topics.length > 0) {
                                    setTopics(response.data.topics);
                                }
                                if(response.data.hasOwnProperty('analytics') && response.data.analytics !== null && response.data.analytics !== undefined && response.data.analytics.length > 0) {
                                    setSessionId(response.data.analytics);
                                }
                            });
                })
    }, []);

    const theme = createTheme({
        palette: {
            mode: darkMode ? 'dark' : 'light'
        },
    });

    const basketStyling = {
        position: 'fixed',
        right: '100px',
        top: '100px'
    };

    function updateDarkMode(darkMode) {
        setDarkMode(darkMode);
        setPreferences({darkMode: darkMode}, 'ThemePreference');
    }

    function requestTrackingPermission() {
        setPreferences({allowTracking: true}, 'TrackingPreference', 'PermissionPresentation')
                .then((response) => {
                    if(response.data.allowed) {
                        setSessionId(response.data.sessionId)
                    }
                });
    }

    return (
            <ThemeProvider theme={theme}>
                <CssBaseline />
                <Container>
                    <Box
                            sx={{
                                display: 'flex',
                                flexDirection: 'row-reverse',
                            }}
                    >
                        <FormGroup>
                            <FormControlLabel control={ <Switch
                                    checked={darkMode}
                                    onChange={() => updateDarkMode(!darkMode)}
                                    inputProps={{ 'aria-label': 'Dark-Mode' }}
                            />} label="Dark Mode" />
                        </FormGroup>
                        <Tooltip title={"My Topics: " + topics.map(topic => topic.title).join(', ')}>
                            <IconButton>
                                <QuestionMarkIcon />
                            </IconButton>
                        </Tooltip>
                        <Tooltip title={
                            <>
                            {sessionId ?
                                    'My SessionId: ' + sessionId :
                                    <span>
                                        <Button variant="contained" color="success" size="small" onClick={requestTrackingPermission}>
                                            Enable tracking
                                        </Button>
                                    </span>}
                            </>
                        }>
                            <IconButton>
                                <AutoGraphIcon />
                            </IconButton>
                        </Tooltip>
                    </Box>
                    <main>
                        <Outlet/>
                    </main>
                    <aside style={basketStyling}>
                        <Basket basket={basket} setBasket={setBasket} />
                    </aside>
                    <Container>
                        <Paper style={{textAlign: 'center', height: 100, marginTop: 50, padding: 15}} elevation={5}>
                            <Typography id="modal-modal-title" variant="h6" component="h2">
                                {getAdContent(topics)}
                            </Typography>
                        </Paper>
                    </Container>
                </Container>
            </ThemeProvider>
    );
}
