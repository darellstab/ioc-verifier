import React, {useState, useEffect} from 'react';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import {getProducts} from "../service/mockDataProvider";
import {ProductCard} from "../components/ProductCard";



export function ListProductsPage({basket, setBasket, addFavourite}) {

    const [products, setProducts] = useState([]);

    useEffect(() => {
        setProducts(getProducts());
    }, []);

    return (
        <Container maxWidth="md">
            <Typography component="h1" variant="h5">Products</Typography>

            <Grid container spacing={2}>
            {products && products.map(product => {
                    return (
                            <Grid item xs={4} key={product.id}>
                                <ProductCard
                                        name={product.name}
                                        description={product.description}
                                        imageUrl={product.imageUrl}
                                        addToBasket={() => setBasket([...basket,product])}
                                        addFavourite={()=> addFavourite(product.id)}
                                />
                            </Grid>
                    );
                })}
            </Grid>
        </Container>
    );
}
