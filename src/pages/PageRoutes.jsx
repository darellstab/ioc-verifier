import React from 'react'
import {Route, Routes} from 'react-router-dom';
import {ListProductsPage} from "./ListProductsPage";
import {PageLayout} from "./PageLayout";
import {NotFoundPage} from "./NotFoundPage";
import {CheckoutPage} from "./CheckoutPage";


export function PageRoutes({basket, setBasket, topics, setTopics, addFavourite, sessionId, setSessionId}) {

    return (
            <Routes>
                <Route path="/" element={<PageLayout basket={basket} setBasket={setBasket} topics={topics} setTopics={setTopics} sessionId={sessionId} setSessionId={setSessionId} />}>
                    <Route index element={<ListProductsPage basket={basket} setBasket={setBasket} addFavourite={addFavourite} />}/>
                    <Route path="checkout" element={<CheckoutPage basket={basket}/>}/>
                    <Route path="*" element={<NotFoundPage/>}/>
                </Route>
            </Routes>
    );
}
