import React, {useEffect, useState} from 'react';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import {requestCredentials} from "../service/chapiService";
import * as polyfill from 'credential-handler-polyfill';
import Grid from '@mui/material/Grid';
import Alert from '@mui/material/Alert';
import {NeededCredentialsBox} from "../components/NeededCredentialsBox";
import {sendOrder} from "../service/api";


const MEDIATOR = 'https://authn.io/mediator' + '?origin=' +
        encodeURIComponent(window.location.origin);

export function CheckoutPage({basket}) {

    const [deliveryCredentials, setDeliveryCredentials] = useState(null);
    const [ageVerification, setAgeVerification] = useState(null);
    const [paymentCredentials, setPaymentCredentials] = useState(null);
    const [verificationFeedback, setVerificationFeedback] = useState(null);

    useEffect(() => {
        polyfill
                .loadOnce(MEDIATOR)
    }, []);

    function getCredentials(credentialType, callback) {
        requestCredentials(credentialType)
                .then(result => {
                    callback(result);
                })
    }

    function placeOrder() {
        sendOrder(basket, [deliveryCredentials.data, ageVerification.data, paymentCredentials.data])
                .then(response => {
                    console.log(response);
                    setVerificationFeedback(response);
                })
                .catch(error => {
                    setVerificationFeedback({orderValid: false, errors: [error.message]})
                });
    }

    return (
            <Container maxWidth="md">
                <Typography component="h1" variant="h5" gutterBottom>Checkout</Typography>

                {verificationFeedback && !verificationFeedback.orderValid &&
                        <Alert style={{marginBottom: '10px'}} severity="error">Order was not placed, invalid credentials.</Alert>
                }

                {verificationFeedback && verificationFeedback.orderValid &&
                        <Alert style={{marginBottom: '10px'}} severity="success">Valid credentials, order placed successfully.</Alert>
                }

                <Grid container spacing={2} mb={4}>
                    <NeededCredentialsBox title="Delivery" reason="Please provide delivery details"
                                          providedCredentials={deliveryCredentials}
                                          getFromWallet={() => getCredentials('DeliveryCredentials', setDeliveryCredentials)}
                                          verificationFeedback={verificationFeedback}
                    />
                    <NeededCredentialsBox title="Age verification" reason="Please provide age verification"
                                          providedCredentials={ageVerification}
                                          getFromWallet={() => getCredentials('AgeCredentials', setAgeVerification)}
                                          verificationFeedback={verificationFeedback}
                    />
                    <NeededCredentialsBox title="Payment" reason="Please provide a payment method"
                                          providedCredentials={paymentCredentials}
                                          getFromWallet={() => getCredentials('PaymentCredentials', setPaymentCredentials)}
                                          verificationFeedback={verificationFeedback}
                    />
                </Grid>
                {deliveryCredentials && ageVerification && paymentCredentials && <Button variant="contained" onClick={placeOrder}>Place Order</Button>}
            </Container>
    );
}
